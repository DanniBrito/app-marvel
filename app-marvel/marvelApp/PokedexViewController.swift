//
//  PokedexViewController.swift
//  marvelApp
//
//  Created by Jairo Vera on 29/11/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import UIKit

class PokedexViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PokemonServiceDelegate {
   
    var pokemonArray:[Pokemon] = []
    var pokemonIndex = 0
    
    @IBOutlet weak var pokedexTableView: UITableView!
    func get20FirstPokemons(pokemons: [Pokemon]) {
        pokemonArray = pokemons
        self.pokedexTableView.reloadData()

        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let service = PokemonService()
        service.delegate = self
        service.downloadPokemons()

        
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        //numero de secciones
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //numero de filas en cada seccion
        return pokemonArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //devuelve la data de cada fila
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
        //cell.textLabel?.text = "\(pokemonArray[indexPath.row].id)" //+ pokemonArray[indexPath.row].name
        cell.fillData(pokemon: pokemonArray[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Pokemon"
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        pokemonIndex = indexPath.row
        return indexPath
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let pokemonDetail = segue.destination as! DetailViewController
        pokemonDetail.pokemon = pokemonArray[pokemonIndex]
    }
    
  
}

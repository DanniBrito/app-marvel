//
//  PokemonService.swift
//  marvelApp
//
//  Created by Jairo Vera on 2/1/18.
//  Copyright © 2018 Jairo Vera. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import AlamofireImage

protocol PokemonServiceDelegate{
    func get20FirstPokemons(pokemons:[Pokemon])
}

class PokemonService{
    
    var delegate:PokemonServiceDelegate?
    
    
    func downloadPokemons(){
        
        var pokemonArray: [Pokemon] = []
        let dpGR = DispatchGroup()
        for i in 1...20{
            dpGR.enter()
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseObject { (response: DataResponse<Pokemon>) in
                
                let pokemon = response.result.value
                pokemonArray.append(pokemon!)
                //print("pokemon \(i)")
                dpGR.leave()
                
            }
            
        }
        dpGR.notify(queue: .main){
            pokemonArray = pokemonArray.sorted(by: {$0.id! < $1.id!})
            self.delegate?.get20FirstPokemons(pokemons: pokemonArray)
        }
        
        
    }
    
    func getPokemonImage(id:Int){
        let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
        Alamofire.request("https://httpbin.org/image/png").responseImage { response in
            debugPrint(response)
            
            print(response.request)
            print(response.response)
            debugPrint(response.result)
            
            if let image = response.result.value {
                print("image downloaded: \(image)")
            }
        }
    }
}

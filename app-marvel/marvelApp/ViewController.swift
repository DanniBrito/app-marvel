//
//  ViewController.swift
//  marvelApp
//
//  Created by Jairo Vera on 28/11/17.
//  Copyright © 2017 Jairo Vera. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper


class ViewController: UIViewController{

    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func consultarButtonPressed(_ sender: Any) {
        
        let pkID = arc4random_uniform(250)+1
        let URL = "https://pokeapi.co/api/v2/pokemon/\(pkID)"
        Alamofire.request(URL).responseObject { (response: DataResponse<Pokemon>) in
            
            let pokemon = response.result.value
            
            DispatchQueue.main.async {
                self.nameLabel.text = pokemon?.name ?? ""
                self.heightLabel.text = "\(pokemon?.height ?? 0)"
                self.weightLabel.text = "\(pokemon?.weight ?? 0)"
            }
            
        }
    }
    
}

